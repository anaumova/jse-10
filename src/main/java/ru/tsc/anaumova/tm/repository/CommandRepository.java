package ru.tsc.anaumova.tm.repository;

import ru.tsc.anaumova.tm.api.repository.ICommandRepository;
import ru.tsc.anaumova.tm.constant.ArgumentConst;
import ru.tsc.anaumova.tm.constant.TerminalConst;
import ru.tsc.anaumova.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show system info."
    );

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show terminal commands."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show command list."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show argument list."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show task list."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Show project list."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects."
    );

    private final Command[] terminalCommands = new Command[]{
            INFO, ABOUT, VERSION, HELP,
            COMMANDS, ARGUMENTS,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return terminalCommands;
    }

}