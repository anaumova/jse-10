package ru.tsc.anaumova.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void clearProjectList();

    void createProject();

}