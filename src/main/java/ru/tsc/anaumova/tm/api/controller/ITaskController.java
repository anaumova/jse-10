package ru.tsc.anaumova.tm.api.controller;

public interface ITaskController {

    void showTaskList();

    void clearTaskList();

    void createTask();

}