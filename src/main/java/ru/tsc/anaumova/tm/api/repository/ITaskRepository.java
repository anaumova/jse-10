package ru.tsc.anaumova.tm.api.repository;

import ru.tsc.anaumova.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    List<Task> findAll();

    void remove(Task task);

    void clear();

}