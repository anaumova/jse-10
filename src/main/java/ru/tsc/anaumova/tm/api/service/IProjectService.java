package ru.tsc.anaumova.tm.api.service;

import ru.tsc.anaumova.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    List<Project> findAll();

    void remove(Project project);

    void clear();

}