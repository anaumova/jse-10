package ru.tsc.anaumova.tm.controller;

import ru.tsc.anaumova.tm.api.controller.ITaskController;
import ru.tsc.anaumova.tm.api.service.ITaskService;
import ru.tsc.anaumova.tm.model.Task;
import ru.tsc.anaumova.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTaskList() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        for (Task task : tasks) {
            if (task == null) continue;
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTaskList() {
        System.out.println("[TASKS CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
